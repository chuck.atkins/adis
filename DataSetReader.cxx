//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "DataSetReader.h"

#include <ios>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include <vtkm/cont/CoordinateSystem.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DynamicCellSet.h>

#include "DataSource.h"
#include "CellSet.h"
#include "CoordinateSystem.h"
#include "Field.h"
#include "Keys.h"

// #include <vtkm/io/writer/VTKDataSetWriter.h>

// #include <iostream>

namespace adis
{
namespace io
{

using DataSourceType = adis::io::DataSource;
using DataSourcesType =
  std::unordered_map<std::string, std::shared_ptr<DataSourceType> >;

class DataSetReader::DataSetReaderImpl
{
public:
  DataSetReaderImpl(const std::string dataModelFileName)
  {
    this->ReadJSONFile(dataModelFileName);
  }

  virtual ~DataSetReaderImpl()
  {
    this->Cleanup();
  }

  void Cleanup()
  {
    this->DataSources.clear();
    this->CoordinateSystem.reset();
    this->CellSet.reset();
  }

  template <typename ValueType>
  void ProcessDataSources(const ValueType& dataSources)
  {
    for(auto& dataSource: dataSources)
    {
      if (!dataSource.IsObject())
      {
        throw std::runtime_error("data_sources must contain data_source objects.");
      }
      if (!dataSource.GetObject().HasMember("name"))
      {
        throw std::runtime_error("data_source objects must have name.");
      }
      std::string name = dataSource.GetObject()["name"].GetString();
      if(name.empty())
      {
        throw std::runtime_error("data_source name must be a non-empty string.");
      }
      if (!dataSource.GetObject().HasMember("filename_mode"))
      {
        throw std::runtime_error("data_source objects must have filename_mode.");
      }
      std::string filename_mode = dataSource.GetObject()["filename_mode"].GetString();
      if(filename_mode.empty())
      {
        throw std::runtime_error("data_source filename_mode must be a non-empty string.");
      }
      auto source = std::make_shared<DataSourceType>();
      if(filename_mode == "input")
      {
        source->Mode = adis::io::FileNameMode::Input;
      }
      else if(filename_mode == "relative")
      {
        source->Mode = adis::io::FileNameMode::Relative;
        if (!dataSource.GetObject().HasMember("filename"))
        {
          throw std::runtime_error("data_source objects must have filename.");
        }
        source->FileName = dataSource.GetObject()["filename"].GetString();
      }
      else
      {
        throw std::runtime_error("data_source filename_mode must be input or relative.");
      }
      this->DataSources[name] = source;
    }
  }

  void ProcessCoordinateSystem(const rapidjson::Value& coordSys)
  {
    this->CoordinateSystem =
      std::make_shared<adis::datamodel::CoordinateSystem>();
    this->CoordinateSystem->ObjectName = "coordinate_system";

    this->CoordinateSystem->ProcessJSON(coordSys, this->DataSources);
  }

  void ProcessCellSet(const rapidjson::Value& cellSet)
  {
    this->CellSet =
      std::make_shared<adis::datamodel::CellSet>();
    this->CellSet->ObjectName = "cell_set";

    this->CellSet->ProcessJSON(cellSet, this->DataSources);
  }

  std::shared_ptr<adis::datamodel::Field>
    ProcessField(const rapidjson::Value& fieldJson)
  {
    if(!fieldJson.IsObject())
    {
      throw std::runtime_error("field needs to be an object.");
    }
    auto field = std::make_shared<adis::datamodel::Field>();
    field->ProcessJSON(fieldJson, this->DataSources);
    field->ObjectName = "field";
    return field;
  }

  void ProcessFields(const rapidjson::Value& fields)
  {
    this->Fields.clear();
    if(!fields.IsArray())
    {
      throw std::runtime_error("fields is not an array.");
    }
    auto fieldsArray = fields.GetArray();
    for(auto& field : fieldsArray)
    {
      auto fieldPtr = this->ProcessField(field);
      this->Fields[std::make_pair(fieldPtr->Name, fieldPtr->Association)] =
        fieldPtr;
    }
  }

  template <typename ValueType>
  const rapidjson::Value& FindAndReturnObject(ValueType& root,
    const std::string name)
  {
    if (!root.HasMember(name.c_str()))
    {
      throw std::runtime_error("Missing " + name + " member.");
    }
    auto& val = root[name.c_str()];
    if (!val.IsObject())
    {
      throw std::runtime_error(name + " is expected to be an object.");
    }
    return val;
  }

  void ReadJSONFile(const std::string dataModelFileName)
  {
    this->Cleanup();

    // Parse the JSON metadata file
    FILE *fp = std::fopen(dataModelFileName.c_str(), "rb");
    if(!fp)
    {
      throw std::ios_base::failure("Unable to open metadata file");
    }

    std::vector<char> buffer(65536);

    rapidjson::FileReadStream is(fp, buffer.data(), buffer.size());

    rapidjson::Document document;
    document.ParseStream(is);
    std::fclose(fp);

    assert(document.IsObject());

    auto m = document.GetObject().begin();
    if(m == document.GetObject().end())
    {
      return;
    }
    if(!m->value.IsObject())
    {
      return;
    }
    const auto obj = m->value.GetObject();
    if (!obj.HasMember("data_sources"))
    {
      throw std::runtime_error("Missing data_sources member.");
    }
    this->ProcessDataSources(obj["data_sources"].GetArray());

    if (!obj.HasMember("coordinate_system"))
    {
      throw std::runtime_error("Missing coordinate_system member.");
    }
    auto& cs = this->FindAndReturnObject(obj, "coordinate_system");
    this->ProcessCoordinateSystem(cs);

    if (!obj.HasMember("cell_set"))
    {
      throw std::runtime_error("Missing cell_set member.");
    }
    auto& cells = this->FindAndReturnObject(obj, "cell_set");
    this->ProcessCellSet(cells);

    if (obj.HasMember("fields"))
    {
      auto& fields = obj["fields"];
      this->ProcessFields(fields);
    }

  }

  std::vector<vtkm::cont::CoordinateSystem> ReadCoordinateSystem(
    const std::unordered_map<std::string, std::string>& paths,
    const adis::metadata::MetaData& selections)
  {
    if(!this->CoordinateSystem)
    {
      throw std::runtime_error("Cannot read missing coordinate system.");
    }
    return this->CoordinateSystem->Read(
      paths, this->DataSources, selections);
  }

  std::vector<vtkm::cont::DynamicCellSet> ReadCellSet(
    const std::unordered_map<std::string, std::string>& paths,
    const adis::metadata::MetaData& selections)
  {
    if(!this->CellSet)
    {
      throw std::runtime_error("Cannot read missing cell set.");
    }
    return this->CellSet->Read(
      paths, this->DataSources, selections);
  }

  adis::metadata::MetaData ReadMetaData(
    const std::unordered_map<std::string, std::string>& paths)
  {
    if(!this->CoordinateSystem)
    {
      throw std::runtime_error("Cannot read missing coordinate system.");
    }
    size_t nBlocks = this->CoordinateSystem->GetNumberOfBlocks(
      paths, this->DataSources);
    adis::metadata::MetaData metaData;
    adis::metadata::Size nBlocksM(nBlocks);
    metaData.Set(adis::keys::NUMBER_OF_BLOCKS(), nBlocksM);

    if (!this->Fields.empty())
    {
      adis::metadata::Vector<adis::metadata::FieldInformation> fields;
      for(auto& item : this->Fields)
      {
        auto& field = item.second;
        adis::metadata::FieldInformation afield(field->Name, field->Association);
        fields.Data.push_back(afield);
      }
      metaData.Set(adis::keys::FIELDS(), fields);
    }

    return metaData;
  }

  void DoAllReads()
  {
    for(auto source : this->DataSources)
    {
      source.second->DoAllReads();
    }
  }

  void BeginStep(
    const std::unordered_map<std::string, std::string>& paths)
  {
    for(auto source : this->DataSources)
    {
      auto& ds = *(source.second);
      std::string name = source.first;
      auto itr = paths.find(name);
      if (itr == paths.end())
      {
        throw std::runtime_error("Could not find data_source with name "
          + name + " among the input paths.");
      }
      std::string path = itr->second + ds.FileName;
      ds.OpenSource(path);
      ds.BeginStep();
    }
  }

  void EndStep()
  {
    for(auto source : this->DataSources)
    {
      source.second->EndStep();
    }
  }

  DataSourcesType DataSources;
  std::shared_ptr<adis::datamodel::CoordinateSystem>
    CoordinateSystem = nullptr;
  std::shared_ptr<adis::datamodel::CellSet> CellSet = nullptr;
  using FieldsKeyType =
    std::pair<std::string, vtkm::cont::Field::Association>;
  std::map<FieldsKeyType, std::shared_ptr<adis::datamodel::Field> > Fields;
};

DataSetReader::DataSetReader(const std::string dataModelFilename)
: Impl(new DataSetReaderImpl(dataModelFilename))
{
}


DataSetReader::~DataSetReader()
{
}

adis::metadata::MetaData DataSetReader::ReadMetaData(
  const std::unordered_map<std::string, std::string>& paths)
{
  return this->Impl->ReadMetaData(paths);
}

vtkm::cont::MultiBlock DataSetReader::ReadDataSet(
  const std::unordered_map<std::string, std::string>& paths,
  const adis::metadata::MetaData& selections)
{
  vtkm::cont::MultiBlock ds = this->ReadDataSetInternal(
    paths, selections);
  this->Impl->DoAllReads();

  // for(size_t i=0; i<ds.GetNumberOfBlocks(); i++)
  // {
  //   vtkm::io::writer::VTKDataSetWriter writer(
  //     "output" + std::to_string(i) + ".vtk");
  //   writer.WriteDataSet(ds.GetBlock(i));
  // }

  return ds;
}

vtkm::cont::MultiBlock DataSetReader::ReadDataSetNextStep(
  const std::unordered_map<std::string, std::string>& paths,
  const adis::metadata::MetaData& selections)
{
  this->Impl->BeginStep(paths);
  vtkm::cont::MultiBlock ds = this->ReadDataSetInternal(
    paths, selections);
  this->Impl->EndStep();

  // for(size_t i=0; i<ds.GetNumberOfBlocks(); i++)
  // {
  //   vtkm::io::writer::VTKDataSetWriter writer(
  //     "output" + std::to_string(i) + ".vtk");
  //   writer.WriteDataSet(ds.GetBlock(i));
  // }

  return ds;
}

vtkm::cont::MultiBlock DataSetReader::ReadDataSetInternal(
  const std::unordered_map<std::string, std::string>& paths,
  const adis::metadata::MetaData& selections)
{
  std::vector<vtkm::cont::CoordinateSystem> coordSystems =
    this->Impl->ReadCoordinateSystem(paths, selections);
  size_t nPartitions = coordSystems.size();
  std::vector<vtkm::cont::DataSet> dataSets(nPartitions);
  std::vector<vtkm::cont::DynamicCellSet> cellSets =
    this->Impl->ReadCellSet(paths, selections);
  for(size_t i=0; i<nPartitions; i++)
  {
    dataSets[i].AddCoordinateSystem(coordSystems[i]);
    dataSets[i].AddCellSet(cellSets[i]);
  }

  if (selections.Has(adis::keys::FIELDS()))
  {
    using FieldInfoType =
      adis::metadata::Vector<adis::metadata::FieldInformation>;
    auto& fields = selections.Get<FieldInfoType>(adis::keys::FIELDS());
    for(auto& field : fields.Data)
    {
      auto itr = this->Impl->Fields.find(
        std::make_pair(field.Name, field.Association));
      if (itr != this->Impl->Fields.end())
      {
        std::vector<vtkm::cont::Field> fieldVec =
          itr->second->Read(paths, this->Impl->DataSources, selections);
        for(size_t i=0; i<nPartitions; i++)
        {
          dataSets[i].AddField(fieldVec[i]);
        }
      }
    }
  }
  else
  {
    for(auto& field : this->Impl->Fields)
    {
      std::vector<vtkm::cont::Field> fields =
        field.second->Read(paths, this->Impl->DataSources, selections);
      for(size_t i=0; i<nPartitions; i++)
      {
        dataSets[i].AddField(fields[i]);
      }
    }
  }

  return vtkm::cont::MultiBlock(dataSets);
}

} // end namespace io
} // end namespace adis
