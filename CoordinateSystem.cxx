//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "CoordinateSystem.h"

namespace adis
{
namespace datamodel
{

void CoordinateSystem::ProcessJSON(const rapidjson::Value& json,
                                   DataSourcesType& sources)
{
  this->Array.reset();
  if (!json.HasMember("array") || !json["array"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide an array object.");
  }
  this->Array = std::make_shared<adis::datamodel::Array>();
  this->Array->ObjectName = "array";
  this->Array->ProcessJSON(json["array"], sources);
}

size_t CoordinateSystem::GetNumberOfBlocks(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources)
{
  return this->Array->GetNumberOfBlocks(paths, sources);
}

std::vector<vtkm::cont::CoordinateSystem> CoordinateSystem::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const adis::metadata::MetaData& selections)
{
  using VecType = vtkm::Vec<float, 3>;
  std::vector<vtkm::cont::DynamicArrayHandle> arrays =
    this->Array->Read(paths, sources, selections);
  std::vector<vtkm::cont::CoordinateSystem> coordSystems;
  coordSystems.reserve(arrays.size());
  using CoordsHandleType = vtkm::cont::ArrayHandle<VecType>;
  for(auto array : arrays)
  {
    CoordsHandleType coordsHandle = array.Cast<CoordsHandleType>();
    coordSystems.push_back(
      vtkm::cont::CoordinateSystem("coordinates", coordsHandle));
  }
  return coordSystems;
}

}
}