//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_CellSet_H_
#define adis_datamodel_CellSet_H_

#include "DataModel.h"

#include <vtkm/cont/DynamicCellSet.h>

namespace adis
{
namespace datamodel
{

/// \brief Data model object for VTK-m cell sets.
///
/// \c adis::datamodel::CellSet is responsible of creating
/// a VTK-m cell set for each block.
struct CellSet : public DataModelBase
{
  /// Reads and returns the cell sets.
  /// The paths are passed to the \c DataSources to create
  /// file paths. \c selections restrict the data that is loaded.
  std::vector<vtkm::cont::DynamicCellSet> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections);

protected:
  std::vector<vtkm::cont::DynamicCellSet> CellSetCache;
};

}
}

#endif