//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "Field.h"

namespace adis
{
namespace datamodel
{

void Field::ProcessJSON(const rapidjson::Value& json,
                                   DataSourcesType& sources)
{
  this->Array.reset();
  if (!json.HasMember("name") || !json["name"].IsString())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a valid name.");
  }
  this->Name = json["name"].GetString();

  if (!json.HasMember("association") || !json["association"].IsString())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a valid association (points or cell_set).");
  }
  const std::string& assoc = json["association"].GetString();
  if (assoc == "points")
  {
    this->Association = vtkm::cont::Field::Association::POINTS;
  }
  else if (assoc == "cell_set")
  {
    this->Association = vtkm::cont::Field::Association::CELL_SET;
  }
  else
  {
    throw std::runtime_error(
      this->ObjectName  + " provided unknown association: " + assoc);
  }

  if (!json.HasMember("array") || !json["array"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide an array object.");
  }
  this->Array = std::make_shared<adis::datamodel::Array>();
  this->Array->ObjectName = "array";
  this->Array->ProcessJSON(json["array"], sources);
}

std::vector<vtkm::cont::Field> Field::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const adis::metadata::MetaData& selections)
{
  std::vector<vtkm::cont::DynamicArrayHandle> arrays =
    this->Array->Read(paths, sources, selections);
  std::vector<vtkm::cont::Field> fields;
  size_t nFields = arrays.size();
  fields.reserve(nFields);
  for(size_t i=0; i<nFields; i++)
  {
    vtkm::cont::Field fld(this->Name, this->Association, arrays[i]);
    fields.push_back(fld);
  }

  return fields;
}

}
}