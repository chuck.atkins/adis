add_executable(test-basic test-basic.cxx)
target_link_libraries(test-basic PRIVATE adis adios2::adios2)
add_test(NAME test-basic COMMAND test-basic ${CMAKE_CURRENT_SOURCE_DIR}/vtk-uns-grid-2.json ${CMAKE_CURRENT_SOURCE_DIR}/data)

add_executable(test-streaming test-streaming.cxx)
target_link_libraries(test-streaming PRIVATE adis adios2::adios2)
add_test(NAME test-streaming COMMAND test-streaming ${CMAKE_CURRENT_SOURCE_DIR}/vtk-uns-grid-2.json ${CMAKE_CURRENT_SOURCE_DIR}/data)

add_executable(test-streaming-separate-sources test-streaming-separate-sources.cxx)
target_link_libraries(test-streaming-separate-sources PRIVATE adis adios2::adios2)
add_test(NAME test-streaming-separate-sources COMMAND test-streaming-separate-sources ${CMAKE_CURRENT_SOURCE_DIR}/vtk-uns-grid-sep.json ${CMAKE_CURRENT_SOURCE_DIR}/data/)

add_executable(test-time test-time.cxx)
target_link_libraries(test-time PRIVATE adis adios2::adios2)
add_test(NAME test-time COMMAND test-time ${CMAKE_CURRENT_SOURCE_DIR}/vtk-uns-grid-2.json ${CMAKE_CURRENT_SOURCE_DIR}/data)
