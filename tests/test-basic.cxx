//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "DataSetReader.h"

#include <mpi.h>
#include <string>
#include <unordered_map>
#include <vector>

#include <vtkm/cont/Algorithm.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/ScatterPermutation.h>

class CheckTopology : public vtkm::worklet::WorkletMapPointToCell
{
public:
  using ControlSignature = void(CellSetIn cellset,
                                FieldInCell<> trueConn,
                                FieldOutCell<> outCells);
  using ExecutionSignature = void(FromIndices, _2, _3);
  using InputDomain = _1;
  using ScatterType = vtkm::worklet::ScatterPermutation<>;

  template <typename ConnType>
  VTKM_EXEC void operator()(const ConnType& inputConn,
                            const vtkm::Id3& trueConn,
                            bool& isTrue) const
  {
    isTrue = true;
    for(int i=0; i<3; i++)
    {
      if (inputConn[i] != trueConn[i])
      {
        isTrue = false;
        break;
      }
    }
  }
};

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);

  int retVal = 0;
  adis::io::DataSetReader reader(argv[1]);
  std::unordered_map<std::string, std::string> paths;
  // paths["the-source"] = "tris.bp";
  paths["the-source"] = std::string(argv[2]) + "/tris-blocks.bp";
  auto metaData = reader.ReadMetaData(paths);
  auto& nBlocks =
    metaData.Get<adis::metadata::Size>(adis::keys::NUMBER_OF_BLOCKS());
  if (nBlocks.NumberOfItems != 2)
  {
    std::cerr << "Error: expected 2 blocks, got " << nBlocks.NumberOfItems << std::endl;
    retVal = 1;
  }
  using FieldInfoType =
    adis::metadata::Vector<adis::metadata::FieldInformation>;
  auto& fields = metaData.Get<FieldInfoType>(adis::keys::FIELDS());
  if (fields.Data.size() != 2)
  {
    std::cerr << "Error: expected 2 arrays, got " << fields.Data.size() << std::endl;
    retVal = 1;
  }
  adis::metadata::MetaData selections;
  adis::metadata::Vector<size_t> blockSelection;
  blockSelection.Data.push_back(1);
  selections.Set(adis::keys::BLOCK_SELECTION(), blockSelection);
  FieldInfoType fieldSelection;
  fieldSelection.Data.push_back(
    adis::metadata::FieldInformation(
      "dpot2", vtkm::cont::Field::Association::POINTS));
  selections.Set(adis::keys::FIELDS(), fieldSelection);
  vtkm::cont::MultiBlock output =
    reader.ReadDataSet(paths, selections);
  if (output.GetNumberOfBlocks() != 1)
  {
    std::cerr << "Error: expected 1 output blocks, got " << output.GetNumberOfBlocks() << std::endl;
    retVal = 1;
  }
  vtkm::cont::DataSet ds = output.GetBlock(0);
  vtkm::IdComponent nFields = ds.GetNumberOfFields();
  if (nFields != 1)
  {
    std::cerr << "Error: expected 1 output arry, got " << nFields << std::endl;
    retVal = 1;
  }
  if (!ds.HasField("dpot2", vtkm::cont::Field::Association::POINTS))
  {
    std::cerr << "Error: expected a dpot2 array. Did not get it." << std::endl;
    retVal = 1;
  }
  std::vector<vtkm::Id3> groundTruth = {{2,8,0}, {8, 14, 0}};
  groundTruth.resize(48);
  vtkm::cont::ArrayHandle<bool> result;
  auto cellSet = ds.GetCellSet(0).Cast<vtkm::cont::CellSetSingleType<> >();
  std::vector<vtkm::Id> cellsToVisit;
  cellsToVisit.push_back(0);
  cellsToVisit.push_back(1);
  vtkm::worklet::ScatterPermutation<> scatter(vtkm::cont::make_ArrayHandle(
    cellsToVisit));
  vtkm::worklet::DispatcherMapTopology<CheckTopology> dispatcher(scatter);
  dispatcher.Invoke(cellSet, vtkm::cont::make_ArrayHandle(groundTruth), result);
  bool ret = vtkm::cont::Algorithm::Reduce(result, true, vtkm::Product{});
  if (!ret)
  {
    std::cerr << "Error: The topology does not match the ground truth." << std::endl;
    retVal = 1;
  }

  MPI_Finalize();
  return retVal;
}
