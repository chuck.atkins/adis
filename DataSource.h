//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_DataSource_H_
#define adis_datamodel_DataSource_H_

#include "MetaData.h"

#include <adios2.h>
#include <mpi.h>
#include <vector>

#include <vtkm/cont/DynamicArrayHandle.h>

namespace adis
{
namespace io
{

enum class FileNameMode
{
  Input,
  Relative
};

/// \brief Data producer for ADIS data models.
///
/// \c adis::io::DataSource is responsible of performing the
/// actual IO operations to load arrays into memory. It produces
/// VTK-m arrays. Only ADIOS2 is currently supported.
struct DataSource
{
  /// \c FileNameMode determines how full file paths are formed
  /// when loading data. When the \c FileNameMode is set to \c Input,
  /// the argument to \c OpenSource() is used directly and the FileName
  /// data member is ignored. When \c FileNameMode is set to \c Relative,
  /// the FileName is appended to the argument to \c OpenSource(). This
  /// enables the use of multiple files all residing in the same path.
  FileNameMode Mode;

  /// Used only when \c FileNameMode is set to \c Relative.
  std::string FileName = "";

  DataSource() = default;
  DataSource& operator=(const DataSource& other)
  {
    if(this != &other)
    {
      this->Mode = other.Mode;
      this->FileName = other.FileName;
    }
    return *this;
  }

  DataSource(const DataSource &other)
  {
    if(this != &other)
    {
      this->Mode = other.Mode;
      this->FileName = other.FileName;
    }
  }

  /// Prepare data source for reading. This needs to be called before
  /// any meta-data or heavy-data operations can be performed.
  void OpenSource(const std::string& fname);

  /// Returns the number of blocks (partitions) available from the
  /// data source for the given variable name.
  size_t GetNumberOfBlocks(const std::string& varName);

  /// Prepares for reading the requested variable.
  /// Applies the provided set of selections to potentially restricted
  /// what is loaded. Actual reading happens when \c DoAllReads() or
  /// \c EndStep() is called.
  std::vector<vtkm::cont::DynamicArrayHandle> ReadVariable(
    const std::string& varName, const adis::metadata::MetaData& selections);

  /// Perform all scheduled reads for this data source. This can be one
  /// or more variables.
  void DoAllReads();

  /// Start the next time step.
  void BeginStep();

  /// Finish the time step. This performas all scheduled reads for this
  /// data source.
  void EndStep();

private:

  std::unique_ptr<adios2::ADIOS> Adios = nullptr;
  adios2::IO AdiosIO;
  adios2::Engine BpReader;
  enum class VarType
  {
    PointData,
    CellData
  };
  std::map<std::string, adios2::Params> AvailVars;
  std::map<std::string, adios2::Params> AvailAtts;
};

}
}

#endif
