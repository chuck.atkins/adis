//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "DataSource.h"

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/Storage.h>

#include <numeric>

#include <iostream>

namespace adis
{
namespace io
{

void DataSource::OpenSource(const std::string& fname)
{
  if(this->Adios)
  {
    return;
  }

  this->Adios.reset(
    new adios2::ADIOS(MPI_COMM_WORLD, adios2::DebugON));

  this->AdiosIO = this->Adios->DeclareIO("adios-io-read");
  this->AdiosIO.SetEngine("bp3");
  this->BpReader = this->AdiosIO.Open(fname, adios2::Mode::Read);

  this->AvailVars =  this->AdiosIO.AvailableVariables();
  this->AvailAtts =  this->AdiosIO.AvailableAttributes();
}

template <typename VariableType, typename VecType>
vtkm::cont::DynamicArrayHandle AllocateArrayHandle(
  size_t bufSize, VariableType*& buffer)
{
  vtkm::cont::internal::Storage<
    VecType, vtkm::cont::StorageTagBasic> storage;
  storage.Allocate(bufSize);
  buffer = reinterpret_cast<VariableType*>(storage.GetArray());
  return vtkm::cont::ArrayHandle<VecType>(std::move(storage));
}

template <typename VariableType>
vtkm::cont::DynamicArrayHandle ReadVariableInternal(
  adios2::Engine& bpReader,
  adios2::Variable<VariableType>& varADIOS2,
  size_t blockId)
{
  auto blocksInfo =  bpReader.BlocksInfo(varADIOS2, 0);
  const auto& shape = blocksInfo[blockId].Count;
  size_t bufSize = 1;
  for(auto n : shape)
  {
    bufSize *= n;
  }

  vtkm::cont::DynamicArrayHandle retVal;
  VariableType* buffer;

  if (shape.size() == 1)
  {
    vtkm::cont::internal::Storage<
      VariableType, vtkm::cont::StorageTagBasic> storage;
    storage.Allocate(bufSize);
    buffer = storage.GetArray();
    vtkm::cont::ArrayHandle<VariableType> arrayHandle(std::move(storage));
    retVal = arrayHandle;
  }
  else if (shape.size() == 2)
  {
    switch(shape[1])
    {
      case 1:
        retVal =
          AllocateArrayHandle<VariableType, VariableType>(
            bufSize, buffer);
        break;
      case 2:
        retVal =
          AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 2> >(
            shape[0], buffer);
        break;
      case 3:
        retVal =
          AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 3> >(
            shape[0], buffer);
        break;
      default:
        break;
    }
  }
  else
  {
  // raise exception here
  }
  bpReader.Get(varADIOS2, buffer);

  return retVal;
}

template <typename VariableType>
size_t GetNumberOfBlocksInternal(
  adios2::IO& adiosIO,
  adios2::Engine& bpReader,
  const std::string& varName)
{
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);
  auto blocksInfo =  bpReader.BlocksInfo(varADIOS2, 0);
  return blocksInfo.size();
}


template <typename VariableType>
std::vector<vtkm::cont::DynamicArrayHandle> ReadVariableBlocksInternal(
  adios2::IO& adiosIO,
  adios2::Engine& bpReader,
  const std::string& varName,
  const adis::metadata::MetaData& selections)
{
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);

  auto blocksInfo =  bpReader.BlocksInfo(varADIOS2, 0);
  std::vector<size_t> blocksToReallyRead;
  if (!selections.Has(adis::keys::BLOCK_SELECTION()) ||
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data.empty())
  {
    size_t nBlocks = blocksInfo.size();
    blocksToReallyRead.resize(nBlocks);
    std::iota(blocksToReallyRead.begin(),
              blocksToReallyRead.end(),
              0);
  }
  else
  {
    const std::vector<size_t>& blocksToRead =
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data;
    blocksToReallyRead = blocksToRead;
  }
  std::vector<vtkm::cont::DynamicArrayHandle> arrays;
  arrays.reserve(blocksToReallyRead.size());

  if (selections.Has(adis::keys::STEP_SELECTION()) &&
      varADIOS2.Steps() > 1)
  {
    size_t stepSel = selections.Get<adis::metadata::Index>(
      adis::keys::STEP_SELECTION()).Data;
    varADIOS2.SetStepSelection({stepSel, 1});
  }
  for(auto blockId : blocksToReallyRead)
  {
    varADIOS2.SetBlockSelection(blockId);
    arrays.push_back(
      ReadVariableInternal<VariableType>(bpReader, varADIOS2, blockId));
  }

  return arrays;
}

#define adisTemplateMacro(call) \
  switch(type[0]) \
  { \
    case 'c': \
    { \
      using adis_TT = char; \
      return call; \
      break; \
    } \
    case 'f': \
    { \
      using adis_TT = float; \
      return call; \
      break; \
    } \
    case 'd': \
    { \
      using adis_TT = double; \
      return call; \
      break; \
    } \
    case 'i': \
    { \
      using adis_TT = int; \
      return call; \
      break; \
    } \
    case 'l': \
      if (type == "long long int") \
      { \
        using adis_TT = vtkm::Id; \
        return call; \
      } \
      else if (type == "long int") \
      { \
        using adis_TT = long int; \
        return call; \
      } \
      break; \
    case 's': \
      if (type == "short") \
      { \
        using adis_TT = long int; \
        return call; \
      } \
      else if (type == "signed char") \
      { \
        using adis_TT = signed char; \
        return call; \
      } \
      break; \
    case 'u': \
      if (type == "unsigned char") \
      { \
        using adis_TT = unsigned char; \
        return call; \
      } \
      else if (type == "unsigned int") \
      { \
        using adis_TT = unsigned int; \
        return call; \
      } \
      else if (type == "unsigned long int") \
      { \
        using adis_TT = unsigned long int; \
        return call; \
      } \
      else if (type == "unsigned long long int") \
      { \
        using adis_TT = unsigned long long int; \
        return call; \
      } \
      break; \
  } \

std::vector<vtkm::cont::DynamicArrayHandle>
  DataSource::ReadVariable(const std::string& varName,
                           const adis::metadata::MetaData& selections)
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variable without an open file.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }
  const std::string& type = itr->second["Type"];
  if(type.empty())
  {
    throw std::runtime_error("Variable type unavailable.");
  }

  adisTemplateMacro(ReadVariableBlocksInternal<adis_TT>(
        this->AdiosIO, this->BpReader, varName, selections));

  throw std::runtime_error("Unsupported variable type " + type);
}

size_t DataSource::GetNumberOfBlocks(const std::string& varName)
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variable without an open file.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }
  const std::string& type = itr->second["Type"];

  adisTemplateMacro(GetNumberOfBlocksInternal<adis_TT>(
      this->AdiosIO, this->BpReader, varName));

  throw std::runtime_error("Unsupported variable type " + type);
}

void DataSource::DoAllReads()
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variables without an open file.");
  }
  this->BpReader.PerformGets();
}

void DataSource::BeginStep()
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variables without an open file.");
  }

  this->BpReader.BeginStep();
}

void DataSource::EndStep()
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variables without an open file.");
  }

  this->BpReader.EndStep();
}

}
}