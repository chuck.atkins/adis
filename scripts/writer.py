import adios2
from mpi4py import MPI
import numpy


adios = adios2.ADIOS(MPI.COMM_WORLD, adios2.DebugON)
bpIO = adios.DeclareIO("BPFile")
bpIO.SetEngine('bp3')


import vtk

sps = vtk.vtkSphereSource()
sps.Update()
pd = sps.GetOutput()
tris = pd.GetPolys()
triIds = tris.GetData()

from vtk.numpy_interface import dataset_adapter as dsa

tris = dsa.vtkDataArrayToVTKArray(triIds)
vtkm_conn = tris.reshape([96,4])[:,1:4].flatten()
conVar = bpIO.DefineVariable("connectivity", vtkm_conn, [288], [0], [288], adios2.ConstantDims)

pts = dsa.vtkDataArrayToVTKArray(pd.GetPoints().GetData())
sp = pts.shape
print(pts)
ptsVar = bpIO.DefineVariable("points", pts, list(sp), [0, 0], list(sp), adios2.ConstantDims)

bpFileWriter = bpIO.Open("tris.bp", adios2.Mode.Write)
bpFileWriter.Put(conVar, vtkm_conn, adios2.Mode.Sync)
bpFileWriter.Put(ptsVar, pts, adios2.Mode.Sync)
bpFileWriter.Close()
