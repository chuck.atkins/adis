//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_metadata_MetaData_H_
#define adis_metadata_MetaData_H_

#include "Keys.h"

#include <vtkm/cont/Field.h>

#include <cstddef>
#include <functional>
#include <unordered_map>

namespace adis
{
namespace metadata
{

/// \brief Superclass for all meta-data classes.
struct MetaDataItem
{
protected:
  // This method needs to be here for the class to
  // support dynamic_cast<>
  virtual void MakePolymorphic() {}
};

/// \brief Meta-data item to store size of things such as number of blocks.
struct Size : public MetaDataItem
{
  Size(size_t nItems) : NumberOfItems(nItems) {}
  size_t NumberOfItems;
};

/// \brief Meta-data item to store an index to a container.
struct Index : public MetaDataItem
{
  Index(size_t idx) : Data(idx) {}
  size_t Data;
};

/// \brief Simple struct representing field information.
struct FieldInformation
{
  FieldInformation(std::string name, vtkm::cont::Field::Association assoc) :
    Name(name), Association(assoc) {}

  // Name of the field.
  std::string Name;
  // Association. See VTK-m field association for details.
  vtkm::cont::Field::Association Association;
};

/// \brief Meta-data item to store a vector.
template<typename T>
struct Vector : public MetaDataItem
{
  Vector(std::vector<T> vec) : Data(vec) {}
  Vector() {}
  std::vector<T> Data;
};

/// \brief Container of meta-data items.
/// This class is a simple wrapper around an std::map that
/// makes setting/getting a bit easier. Internally, it stores
/// objects using unique_ptrs but the interface uses stack
/// objects.
class MetaData
{
public:
  /// Add a meta-data item to the map. Supports subclasses
  /// of \c MetaDataItem only.
  template<typename T>
  void Set(adis::keys::KeyType key, const T& item)
  {
    static_assert( !std::is_same<T, adis::metadata::MetaDataItem>::value, "it is not allowed to pass base type (MetaDataItem) to Set()." );
    std::unique_ptr<T> it(new T(item));
    this->Data[key] = std::move(it);
  }

  /// Given a type, returns an object if it exists.
  /// Raises an exception if the item does not exist
  /// or if the provided template argument is incorrect.
  template<typename T>
  const T& Get(adis::keys::KeyType key) const
  {
    auto itr = this->Data.find(key);
    if(itr == this->Data.end())
    {
      throw std::runtime_error("Item not found.");
    }
    T* value = dynamic_cast<T*>(itr->second.get());
    if (!value)
    {
      throw std::runtime_error("Item is not of requested type.");
    }
    return *value;
  }

  /// Given a key, checks whether an item exists.
  bool Has(adis::keys::KeyType key) const
  {
    auto itr = this->Data.find(key);
    if(itr == this->Data.end())
    {
      return false;
    }
    return true;
  }

private:
  using MetaDataType = std::unordered_map<adis::keys::KeyType,
    std::unique_ptr<MetaDataItem> >;
  MetaDataType Data;
};
}
}

#endif