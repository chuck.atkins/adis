//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_keys_Keys_H_
#define adis_keys_Keys_H_

#include <cstdint>

namespace adis
{
namespace keys
{

using KeyType = std::uintptr_t;

/// Key used for storing number of blocks meta-data.
/// Uses adis::metadata::Size
KeyType NUMBER_OF_BLOCKS();

/// Key used for selecting a set of blocks. Uses
/// adis::metadata::Vector<size_t>
KeyType BLOCK_SELECTION();

/// Key used for available array meta-data and array
/// selection. Uses adis::metadata::Vector<adis::metadata::FieldInformation>
KeyType FIELDS();

/// Key used for selecting time step.
/// Uses adis::metadata::Index
KeyType STEP_SELECTION();

}
}

#endif