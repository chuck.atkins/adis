//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_CoordinateSystem_H_
#define adis_datamodel_CoordinateSystem_H_

#include "Array.h"
#include "DataModel.h"
#include "MetaData.h"

#include <vtkm/cont/CoordinateSystem.h>

namespace adis
{
namespace datamodel
{

/// \brief Data model object for VTK-m coordinate systems.
///
/// \c adis::datamodel::CoordinateSystem is responsible of creating
/// VTK-m coordinate systems by loading data defined by the ADIS
/// data model.
struct CoordinateSystem : public DataModelBase
{
  /// Overridden to handle the undelying Array. The Array
  /// object determines the actual type of the coordinate system.
  virtual void ProcessJSON(const rapidjson::Value& json,
                           DataSourcesType& sources);

  /// Reads and returns coordinate systems. The heavy-lifting is
  /// handled by the underlying Array object.
  /// The paths are passed to the \c DataSources to create
  /// file paths. \c selections restrict the data that is loaded.
  std::vector<vtkm::cont::CoordinateSystem> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections);

  /// Returns the number of blocks in the underlying Array variable.
  /// Used by the reader to provide meta-data on blocks.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources);

private:
  std::shared_ptr<adis::datamodel::Array> Array;
};

}
}

#endif