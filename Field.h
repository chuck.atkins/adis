//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_Field_H_
#define adis_datamodel_Field_H_

#include "DataModel.h"
#include "Array.h"

#include <vtkm/cont/Field.h>

namespace adis
{
namespace datamodel
{

/// \brief Data model object for VTK-m fields.
///
/// \c adis::datamodel::Field is responsible of creating
/// VTK-m fields by loading data defined by the ADIS
/// data model.
struct Field : public DataModelBase
{
  /// Overridden to handle the undelying Array as well as the
  /// association.
  virtual void ProcessJSON(const rapidjson::Value& json,
                           DataSourcesType& sources);

  /// Reads and returns fields. The heavy-lifting is
  /// handled by the underlying Array object.
  /// The paths are passed to the \c DataSources to create
  /// file paths. \c selections restrict the data that is loaded.
  std::vector<vtkm::cont::Field> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections);

  /// Name of the array.
  std::string Name;

  /// The associate of the array. See VTK-m association for
  /// details.
  vtkm::cont::Field::Association Association;

private:
  std::shared_ptr<adis::datamodel::Array> Array;
};

}
}

#endif