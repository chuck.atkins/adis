//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_Array_H_
#define adis_datamodel_Array_H_

#include <vtkm/cont/DynamicArrayHandle.h>

#include "DataModel.h"

namespace adis
{
namespace datamodel
{

/// \brief Data model object for VTK-m array handles.
///
/// \c adis::datamodel::Array is responsible of creating
/// VTK-m \c ArrayHandles by loading data defined by the ADIS
/// data model.
struct Array : public DataModelBase
{
  /// Reads and returns array handles. The heavy-lifting is
  /// handled by the \c DataModelBase \c ReadSelf() method.
  /// The paths are passed to the \c DataSources to create
  /// file paths. \c selections restrict the data that is loaded.
  std::vector<vtkm::cont::DynamicArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections);

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources);
};

}
}

#endif