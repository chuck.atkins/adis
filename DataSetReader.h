//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_io_DataSetReader_h
#define adis_io_DataSetReader_h

#include "MetaData.h"

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/MultiBlock.h>

#include <memory>
#include <string>
#include <unordered_map>

#include "adis_export.h"

namespace adis
{
namespace io
{

/// \brief General purpose reader for data described by an ADIS data model.
///
/// \c adis::io::DataSetReader reads data described by an ADIS data model
/// and creates VTK-m datasets. See the ADIS schema definition for the
/// supported data model. \c DataSetReader also supports reading meta-data.
///
class ADIS_EXPORT DataSetReader
{
public:
  /// Constructor. The \c dataModelFilename argument is the path
  /// to a json file describing the data model to be used by the reader.
  DataSetReader(const std::string dataModelFilename);

  ~DataSetReader();

  /// Read and return meta-data. This includes information such as the
  /// number of blocks, available fields etc.
  adis::metadata::MetaData ReadMetaData(
    const std::unordered_map<std::string,
      std::string>& paths);

  /// Read and return heavy-data. The c paths argument is a map that provides
  /// the paths (filenames usually) corresponding to each data source.
  /// The \c selections argument provides support for reading a subset of
  /// the data by providing choices for things such as time and blocks.
  vtkm::cont::MultiBlock ReadDataSet(
    const std::unordered_map<std::string,
      std::string>& paths,
    const adis::metadata::MetaData& selections);

  /// Same as \c ReadDataSet except that it moves the time step read forward
  /// in every call. It will read the first time when called for the first
  /// time.
  vtkm::cont::MultiBlock ReadDataSetNextStep(
    const std::unordered_map<std::string,
      std::string>& paths,
    const adis::metadata::MetaData& selections);

private:
  class DataSetReaderImpl;
  std::unique_ptr<DataSetReaderImpl> Impl;

  vtkm::cont::MultiBlock ReadDataSetInternal(
    const std::unordered_map<std::string,
      std::string>& paths,
    const adis::metadata::MetaData& selections);
};

} // end namespace io
} // end namespace adis

#endif // adis_io_DataSetReader_h

